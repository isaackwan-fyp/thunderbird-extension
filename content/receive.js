window.addEventListener("load", function(e) {

    function testNotificationButton1Callback(theNotification, buttonInfo, eventTarget) {
        window.alert("Button 1 pressed");
        //Prevent notification from closing:
        return true;
    };

    function testNotificationButton2Callback(theNotification, buttonInfo, eventTarget) {
        window.alert("Button 2 pressed");
        //Do not prevent notification from closing:
    };

    let button1 = {
        isDefault: false,
        label: "Add as Friend",
        callback: testNotificationButton1Callback,
    };

    let button2 = {
        isDefault: true,
        label: "Never Trust This Key",
        callback: testNotificationButton2Callback,
    };

	const nb = document.getElementById("msgNotificationBar");
	GetMessagePane().addEventListener("DOMContentLoaded", function(e){
		console.debug("new content", e);
		if (e.target.domain && !e.target.doctype) {
			nb.appendNotification("Unknown Sender", "signy-unkwown-sender", "chrome://signy-security/skin/identity.png", nb.PRIORITY_INFO_MEDIUM , [button1, button2]);
		}
	}, false);
}, false);