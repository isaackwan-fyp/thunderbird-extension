function send_event_handler(event) {
	const msg_type = document.getElementById("msgcomposeWindow").getAttribute("msgtype");

	// do not continue unless this is an actual send event  
	if (!(msg_type == nsIMsgCompDeliverMode.Now || msg_type == nsIMsgCompDeliverMode.Later)) {
		console.debug("Skipping because this is not an actual send event");
		return;
	}

	if (gMsgCompose.composeHTML !== false) {
		alert("Signy will not proceed because you are not sending a plain text email.");
	}

	gMsgCompose.compFields.setHeader("X-Signy-Signature", "No!");
	GetCurrentEditor().outputToString("text/plain", 4 + 1024 /* OutputRaw + OutputLFLineBreak*/)

	/*try {
		var editor = GetCurrentEditor();
		var editor_type = GetCurrentEditorType();
		editor.beginTransaction();
		editor.beginningOfDocument(); // seek to beginning  
		if (editor_type == "textmail" || editor_type == "text") {
			editor.insertText("foo");
			editor.insertLineBreak();
		} else {
			editor.insertHTML("<p>foo</p>");
		}
		console.log(gMsgCompose, editor);
		editor.endTransaction();
	} catch (ex) {
		Components.utils.reportError(ex);
		return false;
	}*/
}

document.getElementById("msgcomposeWindow").addEventListener("compose-send-message", send_event_handler, true);